/** @file plugin_rhythmbox.h
 *  @brief defines a plugin to control Rhythmbox
 */

#ifndef __BIGHCI_PLUGIN_MPRIS_H__
#define __BIGHCI_PLUGIN_MPRIS_H__

#include <sys/time.h>
#include <rciDaemon/plugin_dbus.h>
#include "org.mpris.MediaPleayer2-glue.h"

namespace Rci
{
class Playlist_Proxy;
class Player_Proxy;
class MprisTemplate;

class Properties_Proxy :
  public org::freedesktop::DBus::Properties_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
private:
  LgCpp::Logger _logger;
  
  Player_Proxy & _playerRef;
  Playlist_Proxy & _playlistRef;

public:
  Properties_Proxy(DBus::Connection &inConnection, Player_Proxy &inPlayer, Playlist_Proxy &inPlaylist, std::string name, std::string service, std::string path) :
    _playerRef(inPlayer), _playlistRef(inPlaylist),
    DBus::ObjectProxy( inConnection, path, service.c_str()), _logger(name + "_Properties_Proxy")
    {}
  ~Properties_Proxy() {}
  
private:
  
  void PropertiesChanged(const std::string &interface_name, const std::map< std::string, ::DBus::Variant > &changed_properties, const std::vector<std::string> &invalidated_properties);

};

class MediaPlayer2_Proxy :
  public org::mpris::MediaPlayer2_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:
  MediaPlayer2_Proxy(DBus::Connection &inConnection,
      std::string name, std::string service, std::string path) :
    DBus::ObjectProxy( inConnection, path, service.c_str() )
    {}
  ~MediaPlayer2_Proxy() {}
};

/** Mpris proxy used to handle the tracklists
 */
class Playlist_Proxy :
  public org::mpris::MediaPlayer2::Playlists_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
public:
friend class Properties_Proxy;
  Playlist_Proxy(DBus::Connection &inConnection, std::string name, std::string service, std::string path) :
    DBus::ObjectProxy( inConnection, path, service.c_str()),
    _logger(name + "_Playlist_Proxy"){;}
  ~Playlist_Proxy()
      {;}
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > > const& getPlaylists()
    {
      if(getPlaylistChanged())
        initPlaylistList();
      return _playlistList;
    }
  void ActivatePlaylistName(std::string inPlaylistName);
  void initPlaylistList();
  
  bool getPlaylistChanged()
  {
    bool ans;
    _mutex.lock();
      ans = _playlistChanged;
      _playlistChanged = false;
    _mutex.unlock();
    return ans;
  }

  std::string getActivePlaylist() { return _activePlaylist; }

public:
  void updateActivePlayList()
  {
    try{
      setActivePlaylist(ActivePlaylist());
    }
    catch( DBus::Error error ){ _logger.logIt("couldn't get the current Playlist", LgCpp::Logger::logINFO); }
  }

private:
  LgCpp::Logger _logger;
  boost::mutex _mutex;    ///< Protects data
  bool _playlistChanged;
  std::string _activePlaylist;
  
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > > _playlistList;

  void PlaylistChanged(const ::DBus::Struct< ::DBus::Path, std::string, std::string >& playlist);
  void setActivePlaylist(const std::string& playlist)
  {
    _activePlaylist = playlist;
  }
  void setActivePlaylist(const ::DBus::Struct< ::DBus::Path, std::string, std::string >& playlist)
  {
    setActivePlaylist(playlist._2);
  }
  void setActivePlaylist(
      const ::DBus::Struct< bool, ::DBus::Struct< ::DBus::Path, std::string, std::string > >&playlist)
  {
    if(playlist._1)
      setActivePlaylist(playlist._2);
    else
      setActivePlaylist("No Playlist Selected");
  }
  void setActivePlaylist(const ::DBus::Variant& playlist)
  {
    ::DBus::Struct< bool, ::DBus::Struct< ::DBus::Path, std::string, std::string > > tmpData;
    DBus::MessageIter tmpIt;
    tmpIt = playlist.reader();
    tmpIt >> tmpData;
    setActivePlaylist(tmpData);
  }
};

class Player_Proxy :
  public org::mpris::MediaPlayer2::Player_proxy,
  public DBus::IntrospectableProxy,
  public DBus::ObjectProxy
{
friend class Properties_Proxy;
private:
  LgCpp::Logger _logger;

  std::map< std::string, ::DBus::Variant > _metadata;

  int64_t _position; ///< position in useconds

  MprisTemplate &_refPlugin;
  boost::mutex p_mutex;

private:
  void Seeked(const int64_t& position){ setPosition(position); }
  void setIsRunning(bool isRunning);
  void updatePositionFromMpris();
  
protected:
  void setMetadata( ::DBus::Variant const& );
  void setMetadata( std::map< std::string, ::DBus::Variant > const& inMetadata){_metadata = inMetadata;}

public:
  Player_Proxy(DBus::Connection &inConnection, std::string name, std::string service, std::string path, MprisTemplate& inPlugin) :
    _refPlugin(inPlugin),
    DBus::ObjectProxy( inConnection, path, service.c_str()), _logger(name + "_Player_Proxy"){;}
  ~Player_Proxy() {}
  
  void updateStatus();
  void updateMetadata();
  
  void setPosition(const int64_t& inPosition);
  int64_t getPosition();
  void updatePosition();
  void setStatus(const std::string& inStatus)
  {
    setIsRunning( (inStatus == "Playing") );
  }
  int getMetadata( std::string const& inKey, std::string &outValue);
};



/** @brief This class defines a Plugin which communicates with rhythmbox using Dbus
 *
 */
class MprisTemplate : public Plugin_Dbus
{
public:
  MprisTemplate(std::string name, std::string service, std::string path);
  virtual ~MprisTemplate();

  void stopFunction();

  virtual int get_Key_Value_Catch( const std::string &inKey, std::string &outValue );
                                        ///< returns the value associated to the key
  virtual void stop_Function();         ///< Stops the plugin (for example stop playing)
  virtual void start_Catch();           ///< Starts the plugin (for example start playing)
  int go_To_State_Catch( const std::string &inState, bool inRepeat, const CmdArguments & inArguments,
                         std::string &outAction, CmdAnswer &outAnswers );
                                        ///< sets the plugin to a given state
  virtual int goToStateMain( std::string &inOutState, const CmdArguments & inArguments,
                             std::string &outAction, CmdAnswer &outAnswers );
                                        ///< main actions to set the plugin to the given state
  
  virtual void eGoToState( const DBus::Error &error, const std::string &inState,
                           std::string &outAction, CmdAnswer &outAnswers );

  virtual void startPositionUpdater();
  virtual void positionUpdater(const boost::system::error_code&);
    ///< called every second to update the position in the media

private:
  boost::asio::deadline_timer* _positionTimer;

protected:
  std::string _service;

  bool msgFilter( const ::DBus::Message & msg ); ///< filters the messages
  Playlist_Proxy     *_playlist;
  Player_Proxy       *_player;
  Properties_Proxy   *_properties;
  MediaPlayer2_Proxy *_mediaplayer;

  int _skipArgument;
  virtual void next( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void previous( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void pause( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void play( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void stop( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void playPause( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void forward( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void backward( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void setVolumeAbs( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void setVolumeRel( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void getPlaylists( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void setPlaylist( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void setFullScreen( bool, CmdArguments const&, std::string *, CmdAnswer * );
  
private:
  void forcePlay();
  
  void seek( bool forward );
  
  int _nConsecutiveSeek;
  bool _previousDirection; //true if forward
  timeval _lastSkeep;
  
  timeval _lastUpdate;
  const timeval _updateInterval;
  std::string _positionStr;
};

}//namespace Rci

#endif //__BIGHCI_PLUGIN_RHYTHMBOX_H__
