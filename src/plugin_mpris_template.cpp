#include <dbus/dbus.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "plugin_mpris_template.h"

void Rci::Properties_Proxy::PropertiesChanged(
    const std::string &interface_name, const std::map< std::string, ::DBus::Variant > &changed_properties,
    const std::vector<std::string> &invalidated_properties)
{
  std::map<std::string, ::DBus::Variant>::const_iterator tmp_ItData;
  
  for(tmp_ItData = changed_properties.begin(); tmp_ItData != changed_properties.end(); ++tmp_ItData)
  {
    if(tmp_ItData->first == "PlaybackStatus")
    {
      _playerRef.setStatus(tmp_ItData->second);
    }
    else if( tmp_ItData->first == "Metadata" ) // updates the metadata
    {
      _playerRef.setMetadata(tmp_ItData->second);
    }
    else if( tmp_ItData->first == "ActivePlaylist")
    {
      _playlistRef.setActivePlaylist(tmp_ItData->second);
    }
    else if( tmp_ItData->first == "PlaylistCount")
    {
      _playlistRef._playlistChanged = true;
    }
  }
  _playerRef.updatePosition();
}

void Rci::Playlist_Proxy::initPlaylistList()
{
  uint32_t nPlaylist = PlaylistCount();
  std::string ordering = Orderings()[0];
  _playlistList = GetPlaylists(0, nPlaylist, ordering, false);
  _activePlaylist = ActivePlaylist()._2._2;
}

/** Activate the playlist with the corresponding name
 *  @param[in] inPlaylistName : name of the playlist to activate
 */
void Rci::Playlist_Proxy::ActivatePlaylistName(std::string inPlaylistName)
{    
  bool found = false;
  
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > >::iterator itPlaylist;
  for( itPlaylist = _playlistList.begin(); itPlaylist != _playlistList.end() && found == false;
       ++itPlaylist )
  {
    if( itPlaylist->_2 == inPlaylistName )
    {
      found = true;
      ActivatePlaylist( itPlaylist->_1 );
    }
  }
}

void Rci::Playlist_Proxy::PlaylistChanged(
                                const ::DBus::Struct< ::DBus::Path, std::string, std::string >& playlist)
{
  bool found = false;
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > >::iterator itPlaylist;

  _activePlaylist = playlist._2;
  for( itPlaylist = _playlistList.begin(); itPlaylist != _playlistList.end() && found == false; ++itPlaylist )
  {
    
    if( itPlaylist->_1 == playlist._1 )
    {
      found = true;
      itPlaylist->_1 = playlist._1;
      itPlaylist->_2 = playlist._2;
      itPlaylist->_3 = playlist._3;
    }
  }
  if( found == false )
  {
    _playlistList.push_back( playlist );
  }
}

void Rci::Player_Proxy::setIsRunning(bool isRunning) 
{ 
  if( isRunning )
  {
    _refPlugin.setRunning();
    _refPlugin.startPositionUpdater();
  }
  else
    _refPlugin.clearRunning();
}

void Rci::Player_Proxy::updatePositionFromMpris()
{
  try{
    setPosition(Position());
  }
  catch(DBus::Error error)
  { 
    setPosition(0);
  }
}

void Rci::Player_Proxy::updateStatus()
{
  try{
    setStatus(PlaybackStatus());
  }
  catch( DBus::Error error ){ _logger.logIt("Status not available", LgCpp::Logger::logINFO); }
}

void Rci::Player_Proxy::updateMetadata()
{
  try{
    setMetadata(Metadata());
  }
  catch( DBus::Error error ){ _logger.logIt("Metadata not available", LgCpp::Logger::logINFO); }
}

void Rci::Player_Proxy::updatePosition()
{
  boost::function<void (void)> f = boost::bind(&Rci::Player_Proxy::updatePositionFromMpris, this);
  _refPlugin.addAsyncAction(f);
}

void Rci::Player_Proxy::setPosition(const int64_t& inPosition)
{
  boost::unique_lock<boost::mutex> lk(p_mutex);
  _position = inPosition;
  _refPlugin.updatePage();
}

void Rci::Player_Proxy::setMetadata( ::DBus::Variant const& inMetadata )
{   
  DBus::MessageIter tmp_It;
  tmp_It = inMetadata.reader();

  tmp_It >> _metadata;

  _refPlugin.updatePage();
}

int64_t Rci::Player_Proxy::getPosition()
{
  int64_t ans;
  boost::unique_lock<boost::mutex> lk(p_mutex);
  ans = _position;
  return ans;
}

int Rci::Player_Proxy::getMetadata( std::string const& inKey, std::string &outValue)
{
  int ans = 0;
  
  std::map< std::string, ::DBus::Variant >::const_iterator tmp_ItData = _metadata.find( inKey );
  
  if( tmp_ItData != _metadata.end())
    outValue = Plugin_Dbus::variant_2_String( tmp_ItData->second );
  else
    ans = -1;

  return ans;
}

Rci::MprisTemplate::MprisTemplate
        (std::string name, std::string service, std::string path) :
        Plugin_Dbus(name),_positionTimer(NULL), _service(service),
        _updateInterval({1,0}),_lastUpdate({0,0}),_lastSkeep({0,0})
{

  setService( _service );
  {
    boost::unique_lock<boost::mutex> lk(Rci::Plugin_Dbus::_dispatcherMutex);    
    _playlist   = new Rci::Playlist_Proxy(*_connectionPlugin, name, service, path);
    _player     = new Rci::Player_Proxy(*_connectionPlugin, name, service, path, *this);
    _properties = new Rci::Properties_Proxy(*_connectionPlugin, *_player, *_playlist, name, service, path);
    _mediaplayer= new Rci::MediaPlayer2_Proxy(*_connectionPlugin, name, service, path);
  }
  
  std::ostringstream ssMessage;
  
  bool waitToStart = true;
  try{
    _connectionPlugin->start_service(service.c_str(), 0);
  }
  catch(DBus::Error error)
  {
    waitToStart = false;
    ///@todo handle the error thrown during start
    // service unknown : name = org.freedesktop.DBus.Error.ServiceUnknown
    // method unknown : org.freedesktop.DBus.Error.UnknownMethod
    ssMessage.str("");
    ssMessage << "error n°" << error.name() << ": " << error.message();
    _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
  }
  
  int i = 0, maxTry = 20;

  while( ( i<maxTry ) && waitToStart )
  {
    try{
      _playlist->initPlaylistList();
      i = maxTry;
    }
    catch( DBus::Error error )
    {
      usleep(1000000);
      ++i;
      ssMessage.str("");
      ssMessage << "waiting " << name << " to start";
      _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
    }
  }
  
  addState( "next", boost::bind(&MprisTemplate::next, this, _1, _2, _3, _4) );
  addState( "previous", boost::bind(&MprisTemplate::previous, this, _1, _2, _3, _4) );
  addState( "pause", boost::bind(&MprisTemplate::pause, this, _1, _2, _3, _4) );
  addState( "play", boost::bind(&MprisTemplate::play, this, _1, _2, _3, _4) );
  addState( "stop", boost::bind(&MprisTemplate::stop, this, _1, _2, _3, _4) );
  addState( "playPause", boost::bind(&MprisTemplate::playPause, this, _1, _2, _3, _4) );
  addState( "forward", boost::bind(&MprisTemplate::forward, this, _1, _2, _3, _4) );
  addState( "backward", boost::bind(&MprisTemplate::backward, this, _1, _2, _3, _4) );
  addState( "fullscreen", boost::bind(&MprisTemplate::setFullScreen, this, _1, _2, _3, _4) );
  addState( "setVolumeAbs", boost::bind(&MprisTemplate::setVolumeAbs, this, _1, _2, _3, _4) );
  addState( "setVolumeRel", boost::bind(&MprisTemplate::setVolumeRel, this, _1, _2, _3, _4) );
  addState( "getPlaylists", boost::bind(&MprisTemplate::getPlaylists, this, _1, _2, _3, _4) );
  addState( "setPlaylist", boost::bind(&MprisTemplate::setPlaylist, this, _1, _2, _3, _4) );
    
  _playlist->updateActivePlayList();
  _player->updateStatus();
  _player->updateMetadata();

  ssMessage.str("");
  ssMessage << "Plugin " << name << " added";
  _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
}

bool Rci::MprisTemplate::msgFilter( const ::DBus::Message & msg ) ///< filters the messages
{
  if(_owner == _serviceMap.end())
    return true;
  return msg.sender() != _owner->second;
}

Rci::MprisTemplate::~MprisTemplate()
{
  _nonBlocking.stop();
  if(_positionTimer != NULL)
  {
    _positionTimer->cancel();
    delete _positionTimer;
  }
  _positionTimer = NULL;
  removeService( _service );
  {
    delete _properties;
    delete _player;
    delete _playlist;
    delete _mediaplayer;
  }
}

int Rci::MprisTemplate::get_Key_Value_Catch(const std::string &inKey, std::string &outValue)
{
  int ans = 0;
  
  if(inKey == "playlist")
    outValue = _playlist->getActivePlaylist();
  else if(inKey == "position")
  {
    int64_t position = _player->getPosition()/1e6;
    unsigned int min = position / 60;
    unsigned int sec = position % 60;
    std::ostringstream ssValue;
    ssValue << min << ":" << sec;
    _positionStr = ssValue.str();
    outValue = _positionStr;
  }
  else
    ans = _player->getMetadata( inKey, outValue ); 
  if( outValue == "" )
    outValue = " --- ";
  
  return ans;
}


void Rci::MprisTemplate::stop_Function()
{
  _player->Stop();
}

/** Commands Plugin to start playing
 */
void Rci::MprisTemplate::start_Catch()
{
  forcePlay();
}

int Rci::MprisTemplate::go_To_State_Catch(const std::string &inState, bool inRepeat,
                                          const CmdArguments & inArguments, std::string& outAction,
                                          CmdAnswer &outAnswers)
{
  outAction = "";
  std::string state = inState;
  
  try{
    if( checkRepeatAction( inState, inRepeat ) )
      goToStateMain(state, inArguments, outAction, outAnswers);
  }
  catch(DBus::Error error)
  {
    eGoToState(error, state, outAction, outAnswers);
  }
  
  return 0;
}

void Rci::MprisTemplate::seek( bool forward )
{
  long int stepModifier;
  long int baseStep = 2000000;
  timeval skeepReset = {3,0};
  
  if( forward == _previousDirection && !checkTimer( _lastSkeep, skeepReset ))
  {
    gettimeofday(&_lastSkeep, NULL);
    ++_nConsecutiveSeek;
  }
  else
  {
    _previousDirection = forward;
    _nConsecutiveSeek = 1;
  }

  stepModifier = (_nConsecutiveSeek > 3)? _nConsecutiveSeek/3 + 1 : 1;
  stepModifier *= forward? 1:-1;
  _player->Seek( stepModifier * baseStep );
}

void Rci::MprisTemplate::next( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->Next(); }
void Rci::MprisTemplate::previous( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->Previous(); }
void Rci::MprisTemplate::pause( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->Pause(); }
void Rci::MprisTemplate::stop( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->Stop(); }
void Rci::MprisTemplate::play( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->Play(); }
void Rci::MprisTemplate::playPause( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ _player->PlayPause(); }
void Rci::MprisTemplate::forward( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ seek(true); }
void Rci::MprisTemplate::backward( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ seek(false); }
void Rci::MprisTemplate::setVolumeAbs( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  if( inArguments.size() == 1 + _skipArgument )
  {
    std::istringstream convert;
    convert.str(inArguments[0 + _skipArgument]);
    double volume;
    convert >> volume;
    volume = (volume < 0.)? 0. : (volume > 1.)? 1. : volume;
    _player->Volume( volume );
  }
  else
    _logger.logIt("Argument(s) missing", LgCpp::Logger::logWARNING);
}
void Rci::MprisTemplate::setVolumeRel( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  if( inArguments.size() == 1 + _skipArgument )
  {
    std::istringstream convert;
    convert.str(inArguments[0 + _skipArgument]);
    double volume;
    convert >> volume;
    volume += _player->Volume();
    volume = (volume < 0.)? 0. : (volume > 1.)? 1. : volume;
    _player->Volume( volume );
  }
  else
    _logger.logIt("Argument(s) missing", LgCpp::Logger::logWARNING);
}
void Rci::MprisTemplate::getPlaylists( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  *outAction = "setPlaylist";
  _logger.logIt(*outAction, LgCpp::Logger::logWARNING);
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > >
        playlist = _playlist->getPlaylists();
  std::vector< ::DBus::Struct< ::DBus::Path, std::string, std::string > >::const_iterator
        itPlaylist;
  for(  itPlaylist = playlist.begin(); itPlaylist != playlist.end(); ++itPlaylist  )
    outAnswers->push_back( itPlaylist->_2 );
}

void Rci::MprisTemplate::setPlaylist( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  if( inArguments.size() == 1 + _skipArgument )
  {
    set_SysTitle("Playlist");
    set_SysValue(inArguments[0 + _skipArgument]);
    set_RequestSysPage();
    _playlist->ActivatePlaylistName( inArguments[0 + _skipArgument] );
  }
  else
    _logger.logIt("Argument(s) missing", LgCpp::Logger::logWARNING);
}

void Rci::MprisTemplate::setFullScreen( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  if(_mediaplayer->CanSetFullscreen())
  {
    bool newState = !_mediaplayer->Fullscreen();
    _mediaplayer->Fullscreen( newState );
  }
}

int Rci::MprisTemplate::goToStateMain( std::string & inOutState, CmdArguments const& inArguments,
                                std::string &outAction, CmdAnswer &outAnswers)
{
  _skipArgument = 0;
  std::ostringstream ssMessage;
  ssMessage << "going to state " << inOutState;
  _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
  
  // action request from the device.
  if(inOutState == "actionRequest" )
  {
    if( inArguments.size() > 0 )
    {
      _skipArgument = 1;
      inOutState = inArguments[0];
      ssMessage.str("");
      ssMessage << "requested state " << inOutState;
      _logger.logIt(ssMessage, LgCpp::Logger::logINFO);
    }
  }
  Rci::Plugin_Base::go_To_State( inOutState, false, inArguments, outAction, outAnswers);
}

void Rci::MprisTemplate::eGoToState(const DBus::Error &error, const std::string &inState,
                           std::string &outAction, CmdAnswer &outAnswers)
{
  std::ostringstream ssMessage;
  ssMessage << "error during action " << inState << std::endl
            << error.message();
  _logger.logIt(ssMessage, LgCpp::Logger::logWARNING);
  std::string state;
  CmdArguments args;
  bool recoverError = true;
  if( inState == "play" || inState == "next" )
    state = "getPlaylists";
  else
    recoverError = false;
  if(recoverError)
    goToStateMain( state, args, outAction, outAnswers );
}

void Rci::MprisTemplate::startPositionUpdater()
{
  if(_positionTimer == NULL)
    _positionTimer = new boost::asio::deadline_timer(_nonBlocking.getIoService());
  if(_positionTimer != NULL && getRunning() == this)
  {
    _positionTimer->expires_from_now( boost::posix_time::seconds(1) );
    _positionTimer->async_wait( boost::bind(&Rci::MprisTemplate::positionUpdater, this, _1) );
  }
  _player->updatePosition();
}

void Rci::MprisTemplate::positionUpdater(const boost::system::error_code& e)
{
  if(_positionTimer != NULL && getRunning() == this && !e )
  {
    _player->setPosition( _player->getPosition() + 1e6 );
    _positionTimer->expires_at( _positionTimer->expires_at() + boost::posix_time::seconds(1) );
    _positionTimer->async_wait(boost::bind(&Rci::MprisTemplate::positionUpdater, this, _1));
  }
}
